package com.maxima.springboot.service;

 import com.maxima.springboot.model.Animal;

 import java.util.List;
 import java.util.Optional;

public interface AnimalService {

    Animal addAnimal (String name, String photoUrl);
    Optional<Animal> getAnimalById(Long id);
    List<Animal> getAllAnimals();
    Animal saveAnimal(Animal animal);
    void deleteAnimalById(Long id);
    void likeAnimal(Long id);
    List<Animal> getTopAnimals(int count); // Вывести первые 10
}
