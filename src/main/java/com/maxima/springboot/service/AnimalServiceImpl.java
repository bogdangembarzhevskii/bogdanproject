package com.maxima.springboot.service;

import com.maxima.springboot.model.Animal;
import com.maxima.springboot.repository.AnimalRepository;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class AnimalServiceImpl implements AnimalService {


    private AnimalRepository animalRepository;

    @Override
    public Animal addAnimal(String name, String photoUrl) {
        Animal newAnimal = new Animal();
        newAnimal.setName(name);
        newAnimal.setPhotoUrl(photoUrl);
        newAnimal.setLikes(0);

        return animalRepository.save(newAnimal);
    }

    @Override
    public Optional<Animal> getAnimalById(Long id) {
        return animalRepository.findById(id);
    }

    @Override
    public List<Animal> getAllAnimals() {
        return animalRepository.findAll();
    }

    @Override
    public Animal saveAnimal(Animal animal) {
        return animalRepository.save(animal);
    }

    @Override
    public void deleteAnimalById(Long id) {
        animalRepository.deleteById(id);
    }

    @Override
    public void likeAnimal(Long id) {
        Optional<Animal> optionalAnimal = animalRepository.findById(id);
        if (optionalAnimal.isPresent()) {
            Animal animal = optionalAnimal.get();
            animal.setLikes(animal.getLikes() + 1); // Добавит коту лайк (нач. 0)
            animalRepository.save(animal);
        } else {
            System.out.println("Животное не найдено! ");
        }
    }

    @Override
    public List<Animal> getTopAnimals(int count) {
        return animalRepository.findTop10AnimalsByLikes(count);
    }
}
