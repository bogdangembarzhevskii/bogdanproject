package com.maxima.springboot.service;

 import com.maxima.springboot.model.Animal;
import com.maxima.springboot.model.User;
import com.maxima.springboot.model.Vote;
import com.maxima.springboot.repository.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VoteService {

    @Autowired
    private VoteRepository voteRepository;

    public void vote(User user, Animal animal) {

        if (voteRepository.existsByUserAndAnimal(user, animal)) {
            throw new IllegalArgumentException("Вы уже проголосовали за этого кота!");
        }

        Vote vote = new Vote();
        vote.setUser(user);
        vote.setAnimal(animal);
        voteRepository.save(vote);
    }

}
